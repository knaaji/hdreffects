bool doBloom = false;
bool doLensFlare = false;
bool doToneMapping = false;
bool doCubicLens = false;
float brightThreshold = 3.0f;
float bloomFactor = 0.2f;
int blurPass = 5;
int addNoise = 0;
