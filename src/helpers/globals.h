#ifndef GLOBALS_H
#define GLOBALS_H

extern bool doBloom;
extern bool doToneMapping;
extern bool doLensFlare;
extern bool doCubicLens;
extern float brightThreshold;
extern float bloomFactor;
extern int blurPass;
extern int addNoise;

#endif
